# desafio-sinatra

# Get started

Install rvm for your operational system. For Ubuntu, check: https://github.com/rvm/ubuntu_rvm

- Install ruby version 3.1.0:
`rvm install ruby 3.1.0`

- Install bundler:
`gem install bundler`

- Clone this repository:
`git clone git@gitlab.com:debora.barussi/desafio-sinatra.git`

- Install the dependencies:
`bundle install`

- To execute the application run:
`ruby app.rb`

- To execute the tests run:
`bundle exec rspec`
