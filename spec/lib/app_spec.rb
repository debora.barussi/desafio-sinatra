# frozen_string_literal: true

require_relative '../../app'
require 'spec_helper'
require 'rack/test'

describe 'app' do
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  context 'GET /index' do
    it 'returns status 200' do
      get '/'
      expect(last_response.status).to eq(200)
    end
  end

  context 'GET /projects' do
    it 'returns status 200' do
      get '/projects'
      expect(last_response.status).to eq(200)
    end
  end

  context 'GET /education' do
    it 'returns status 200' do
      get '/education'
      expect(last_response.status).to eq(200)
    end
  end

  context 'GET /fake-page' do
    it 'returns status 404' do
      get '/fake-page'
      expect(last_response.status).to eq(404)
    end
  end
end
